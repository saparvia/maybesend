#!/bin/bash
cgi_decode()
{
    [ $# -ne 1 ] && return
    local v t h
    # replace all + with whitespace and append %%
    t="${1//+/ }%%"
    while [ ${#t} -gt 0 -a "${t}" != "%" ]; do
    v="${v}${t%%\%*}" # digest up to the first %
    t="${t#*%}"       # remove digested part
    # decode if there is anything to decode and if not at end of string
    if [ ${#t} -gt 0 -a "${t}" != "%" ]; then
        h=${t:0:2} # save first two chars
        t="${t:2}" # remove these
        v="${v}"`echo -e \\\\x${h}` # convert hex to special char
    fi
    done
    # return decoded string
    echo "${v}"
    return
}
#echo "Status: 200 OK"
private_key=`cat /etc/maybesend_captcha_key`
echo "Content-type: text/html"
echo ""
echo "<!DOCTYPE html>"
echo "<html>"

shasum=`dd if=/dev/urandom bs=1 count=16 | sha1sum | cut -d " " -f 1`
filename=`mktemp "/tmp/${shasum}XXX"`
password=`dd if=/dev/urandom bs=1 count=16 | sha1sum | cut -d " " -f 1`
content=`cat -`
fields=`echo "$content" | sed "s/[&=]/ /g"`
prev=""
for field in $fields; do
	case $prev in
	"recaptcha_challenge_field" )
		recaptcha_challenge_field=$field
		;;
	"recaptcha_response_field" )
		recaptcha_response_field=$field
		;;
	"sender" )
		sender=`cgi_decode "$field"`
		;;
	esac
	prev=$field
done
status=`curl --data "privatekey=$private_key&remoteip=$REMOTE_ADDR&challenge=$recaptcha_challenge_field&response=$recaptcha_response_field" http://www.google.com/recaptcha/api/verify | head -n 1`
if [[ "$status" == "true" ]]; then
	echo "<head><title></title><body><h1>Confirmation mail sent</h1><p>Please check your inbox.</p></body>"
	message="Visit the following page to send the mail: http://maybesend.net/cgi-bin/confirm.sh?id=`basename $filename`&p=$password"
	echo "$message" | REPLYTO=donotreply@maybesend.net mail -aFrom:donotreply@maybesend.net -s "Confirm message" "$sender"
	echo -e "$password\n$content" | gpg --homedir /tmp --batch --no-tty --passphrase-fd 0 --symmetric >$filename
else
	echo "<head><title>Incorrect captcha</title></head><body><h1>Incorrect captcha</h1><p>The captcha you entered was incorrect. <a href='javascript:history.go(-1);'>Try again</a>.</body>"
fi
echo "</html>"
