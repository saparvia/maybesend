#!/bin/bash
cgi_decode()
{
    [ $# -ne 1 ] && return
    local v t h
    # replace all + with whitespace and append %%
    t="${1//+/ }%%"
    while [ ${#t} -gt 0 -a "${t}" != "%" ]; do
    v="${v}${t%%\%*}" # digest up to the first %
    t="${t#*%}"       # remove digested part
    # decode if there is anything to decode and if not at end of string
    if [ ${#t} -gt 0 -a "${t}" != "%" ]; then
        h=${t:0:2} # save first two chars
        t="${t:2}" # remove these
        v="${v}"`echo -e \\\\x${h}` # convert hex to special char
    fi
    done
    # return decoded string
    echo "${v}"
    return
}
echo "Content-type: text/html"
echo ""
id=`echo "$QUERY_STRING" | grep -Po '(?<=id=)[a-zA-Z0-9]+'`
password=`echo "$QUERY_STRING" | grep -Po '(?<=p=)[a-zA-Z0-9]+'`
echo "<!DOCTYPE html>"
echo "<html>"

# Sending a file where part of the file name comes from user input? What could possibly go wrong?!
content=`echo "$password" | cat - "/tmp/$id" | gpg --homedir /tmp --batch --no-tty --passphrase-fd 0 -d | sed "s/[&=]/ /g"`
if [[ "$content" == "" ]]; then
echo "<html><head><title>Message already sent (maybe)</title></head><body><h1>Message already sent (maybe)</h1><p>You've already tried to send that message earlier. Maybe it was actually sent, maybe it wasn't. We're not telling you.</p></body></html>"
exit 1
fi
#rm "/tmp/$id"
prev=""
for field in $content; do
case $prev in
	"sender" )
		sender=`cgi_decode "$field"`
		;;
	"recipient" )
		recipient=`cgi_decode "$field"`
		;;
	"subject" )
		subject=`cgi_decode "$field"`
		;;
	"email_message" )
		email_message=`cgi_decode "$field" | sed "s/\r/\n/g"` # I don't really understand why we only get \r instead of \r\n. Probably a BUG in cgi_decode that makes the newlines disappear.
		;;
	"probability" )
		probability=`cgi_decode "$field"`
		;;
	esac
	prev=$field
done
if [ "`awk -v \"p=$probability\" 'BEGIN {srand(); print rand()<p}'`" -eq "1" ]; then 
echo "$email_message" | REPLYTO="$sender" mail "-aFrom:$sender" "-s$subject" "$recipient"
fi

echo "<head><title>Message maybe sent</title></head><body><h1>Message maybe sent</h1><p>Maybe the message was sent, maybe not. We're not telling you.</p></body>"
echo "</html>"
